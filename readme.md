# See Wiki

https://bitbucket.org/stevenV96/productdescriptiondump/wiki/

# Summary

This app dumps all the products description of the collection 'products' into a brand new collection 'productsDescription'. One row per each product description.

![pdd.JPG](https://bitbucket.org/repo/BgA4qkr/images/1816532672-pdd.JPG)
