module.exports = class TicketSuper {

    constructor(rawSuper) {
        if (rawSuper){
            //    this.rawSuper = rawSuper;
            this.name = rawSuper.nombre.toString().toUpperCase();
            this.code = rawSuper.codigo.toString().toUpperCase();
            this.id = rawSuper._id;
            this.insignias = [];
        }
    }
};
