const {MongoClient, ObjectId} = require('mongodb');
const TicketSuper = require('./ticketSuper');
const nyanProgress = require('nyan-progress');

// Connection URL
const url = 'mongodb://root:1234@localhost/rdh';
const DATABASE_NAME = 'rdh';

console.log('Connecting to -> ' + url);

MongoClient.connect(url, {useNewUrlParser: true}, async function (err, client) {
    if (err) {
        console.error(err);
        process.exit(1);
    }

    console.log('Connected to -> ' + url);

    const progress = nyanProgress();
    const activeSupersDictionary = {};

    const clientDb = client.db(DATABASE_NAME);
    const activeSupersMatrix = await getActiveSupers(clientDb);
    activeSupersMatrix.forEach(su => {

        const ticketSuper = new TicketSuper(su);
        activeSupersDictionary[ticketSuper.name] = ticketSuper;
    });

    activeSupersDictionary['ELCORTEINGLES'] = activeSupersDictionary['ECI'];
    activeSupersDictionary['SUMA'] = activeSupersDictionary['SPAR'];

    activeSupersDictionary['ELCORTEINGLES'].insignias = ['SUPERCOR', 'HIPERCOR'];
    activeSupersDictionary['DIA'].insignias = ['CLAREL'];

    const totalProducts = await clientDb.collection('products').countDocuments();

    progress.start({
        total: totalProducts, width: 120, message: {
            downloading: ['Writing product.', 'Writing product..', 'Writing product...'],
            finished: ['Finished'],
            error: ['An unknown error occurred']
        }
    }, () => {

        console.log('Finished writing products!');
        console.log('Closing Database Connection...');

        client.close();

        console.log('Database connection closed!');
        process.exit(0);
    });

    const limit = 1000;
    const totalSegments = Math.round(totalProducts / limit);

    for (let segment = 0; segment < totalSegments; segment++) {

        const offset = segment * limit;
        const products = await getProducts(clientDb, limit, offset);

        for (const productJSON of products) {

            const productId = productJSON['_id'];
            const numAssigned = productJSON['assigned'] || 0;
            const descModifiedBy = productJSON['lastModifiedBy'] || '' ;
            const descDateAdded = productJSON['lastModifiedDate'] ;
            const active = productJSON['activo'] || false;

            const productSuperDescriptions = Object.keys(productJSON).filter(p => /^descripcion/.test(p)).slice(2);

            for (const psdKey of productSuperDescriptions) {
                const descKey = psdKey;
                const descValue = productJSON[psdKey];
                const firstWord = descValue.split(' ')[0] || descValue;
                let superName = descKey.split('descripcion')[1].toString().toUpperCase() || descKey.toString().toUpperCase();

                let language = languages.es_es;

                if (catalanSupersMatrix.includes(superName)) {
                    language = languages.cat_es;
                }

                if (language !== languages.cat_es && superName.endsWith('ES') && superName !== 'ELCORTEINGLES') {

                    // replace ES from superName so BONAREAES becomes BONAREA
                    superName = superName.substring(0, superName.length - 2);
                }

                let oClient = findClient(activeSupersDictionary, superName);

                const clientId = oClient.id;
                //    const clientName = clientObject.name;

                await insertRow(clientDb, {
                    superName: descKey,
                    description: descValue,
                    client: clientId,
                    language: language,
                    productId: productId,
                    dateDescription: descDateAdded,
                    userDescription: descModifiedBy,
                    firstWord: firstWord,
                    numAssigned: numAssigned,
                    active : active
                });
            }

            progress.tick(1);
        }
    }
});

const findClient = (dict, superName) => {
    let resultClientObj;
    if (dict.hasOwnProperty(superName)) {
        resultClientObj = dict[superName];
    } else {
        // console.log(`< Super ${superName} no encontrado. Buscando entre las insignias de los supers >`);
        const value = Object.values(dict).map(value => value).filter(v => v.insignias.includes(superName));
        resultClientObj = value[0];

        /*   if (value) {
               //   console.log('   Super Padre Encontrado -> ' + resultClientObj.name);
           } else {
               // console.error(` Super Padre de ${superName} NO Encontrado`);
           }*/
    }

    return resultClientObj;
};

const insertRow = async (db, params) => {
    //   console.log(params.superName);
    const document = {
        /*        superName: params.superName,*/
        description: params.description,
        clientId: params.client,
        language: params.language,
        productId: params.productId,
        userDescription: params.userDescription,
        firstWord: params.firstWord,
        numAssigned: params.numAssigned,
        active: params.active
    };

    if (params.dateDescription){
        document['dateDescription'] = params.dateDescription;
    }
    // console.log(document);
    await db.collection('productsDescription').insertOne(document);
};

const languages = {
    es_es: 'es_ES',
    cat_es: 'cat_ES'
};

const catalanSupersMatrix = [

    'Condis', 'Keisy', 'Sorli', 'Bonarea', 'Jespac', 'Caprabo', 'Bonpreu', 'Consum', 'Mercadona'
].map(su => su.toString().toUpperCase());

const getActiveSupers = async (db) => {

    // Clientes Reales (Mercadona, Consum...)
    const clientsRetail = await getRetailClients(db);

    // All Ticket Supers
    const supers = await getTicketSupers(db);

    // MARCA PRINCIPAL : INSIGNIAS
    // DIA -> CLAREL
    // EL CORTE INGLES -> SUPERCOR, HIPERCOR
    supers.push('ECI', 'CLAREL', 'SUPERCOR', 'HIPERCOR', 'SPAR');

    // Supers filtrados. Son los supers activos y los que se muestran actualmente
    return clientsRetail.filter(c => {
        const nombre = c['nombre'];
        const result = supers.filter(name => name === nombre);
        return result.length >= 1;
    });
};

const getRetailClients = (db) => (
    db.collection('clientes').find({tipo: 'RETAIL'})
        .toArray()
);

const getTicketSupers = (db) => (
    db.collection('ticketsupers').find().map(su => su.code.toString().toUpperCase())
        .toArray()
);

const getProducts = (db, limit, offset) => (
    db.collection('products').find({}/*{'_id': ObjectId('5c4ae608b600f5b606de86d3')}*/)
        .limit(limit ? limit : 1000)
        .skip(offset ? offset : 0)
        .toArray()
);





